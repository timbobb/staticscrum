from django.contrib import admin

from .models import ScrumyGoals, ScrumyUser, Status, ScrumyHistory



admin.site.register(ScrumyUser)

admin.site.register(ScrumyGoals)

admin.site.register(Status)

admin.site.register(ScrumyHistory)

