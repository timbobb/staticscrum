from .models import ScrumyUser, ScrumyGoals, Status
from rest_framework import serializers


class ScrumyUsersSerializer(serializers.HyperlinkedModelSerializer):

	class Meta:
		model = ScrumyUser
		fields = ('url','userName')

class ScrumyGoalsSerializer(serializers.HyperlinkedModelSerializer):

	class Meta:
		model = ScrumyGoals
		fields = ('url','task_id','task', 'user_id', 'status_id')


class ScrumyStatusSerializer(serializers.HyperlinkedModelSerializer):

	class Meta:
		model = Status
		fields = ('url','status_name')