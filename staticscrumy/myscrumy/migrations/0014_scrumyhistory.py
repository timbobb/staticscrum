# Generated by Django 2.0.1 on 2018-04-04 00:26

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('myscrumy', '0013_scrumygoals_owner'),
    ]

    operations = [
        migrations.CreateModel(
            name='ScrumyHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('moved_by', models.CharField(default='not been moved', max_length=50)),
                ('created_by', models.CharField(max_length=50)),
                ('moved_from', models.IntegerField(default=1234)),
                ('moved_to', models.IntegerField(default=1234)),
                ('time_of_status_change', models.DateTimeField(default=django.utils.timezone.now)),
                ('task_id', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='myscrumy.ScrumyGoals')),
            ],
        ),
    ]
