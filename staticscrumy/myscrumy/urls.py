from django.conf.urls import include, url
from django.urls import path, re_path
from . import views
from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'users', views.ScrumyUsersViewSet)
router.register(r'goals', views.ScrumyGoalsViewSet)
router.register(r'status', views.ScrumyStatusViewSet)



urlpatterns = [
    re_path(r'^$',views.landing_view, name = 'landing'),
    re_path(r'^index$',views.index, name = 'index'),
    re_path(r'^add_user$',views.add_user, name = 'add_user'),
    re_path(r'^add_task$',views.add_task, name = 'add_task'),
    path('delete/<int:task_id>',views.delete_task, name = 'delete_task'),
    path('history/<int:task_pk>',views.history, name = 'history'),
    path('move_task/<int:task_id>',views.move_task, name = 'move_task'),
    path('assign/owner/<int:task_id>',views.assign_task_owner, name = 'assign_owner'),
    re_path(r'^move_task/no/permission$',views.no_permission, name = 'no_permission'),


    re_path(r'^dynamicscrumy$', views.FrontendRenderView, name='home'),
    re_path(r'^', include(router.urls)),
    re_path(r'^api-auth', include('rest_framework.urls', namespace='rest_framework')),
]

'''
urlpatterns += [
    re_path(r'(?P<path>.*)', views.FrontendRenderView.as_view(), name='home')
]
'''